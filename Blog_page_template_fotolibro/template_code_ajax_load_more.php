<div class="post-wrapper post-<?php echo $alm_item; ?>">
	<div class="header-img">
		<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
			<?php
				if ( has_post_thumbnail() ) {
					the_post_thumbnail('full');
				}
			?>
		</a>
	</div>
	<div class="post-content">
		<h2><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
		<div class="excerpt">
			<?php the_excerpt(); ?>
		</div>
	</div>
</div>