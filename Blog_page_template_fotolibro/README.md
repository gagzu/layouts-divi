# Requerimientos

- Se debe instalar el plugins [Ajax load more]

## Pasos para configurar [Ajax load more]

- Ir a configuración global
    - Cambiar [Tipo de contenedor] a `<div>`
    - Cambiar el color del botón si corresponda [Button/Loading Style]
    - El resto de configuraciones son opcionales (se pueden dejar por defecto)

- Ir a [Repeater Templates] y copiar el fragmento de código del fichero `./template_code_ajax_load_more.php`

## Otras configuraciones generales pero no menos importantes

- Es requerido cargar la imagen destacada de las notas.
- Es requerido rellenar en las notas la info del (extracto)
- Para mantener un estilo uniforme la imagen destacada de todas las notas deben tener las mismas medidas y el extracto que sea lo más similar posible en longitud
- Si necesitas cambiar lo que se carga en el tempplate del blog importado debes modificar el shortcode (ver [shortcode builder] en la configuración de ajax load more) por ejemplo solo mostrar una categoría especifica:
    - [ajax_load_more post_type="post" category="mi-categoria"]
- agregar en los estilos de la página del blog el contenido de `./blog.css` para darle un poco de de belleza a la página